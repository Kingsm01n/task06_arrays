package classes;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Container<Product>{
    ArrayList<? super Product> arrayList= new ArrayList<>();
    public void setItem(Product product) {
        this.arrayList.add(product);
    }
    ArrayList<? extends Product> arrayList1 = (ArrayList<? extends Product>) arrayList;
    public Product getItem(int i) {
        return arrayList1.get(i);
    }
}
