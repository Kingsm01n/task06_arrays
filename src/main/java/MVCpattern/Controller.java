package MVCpattern;

import classes.*;
import classes.Container;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Controller {
    public static void start(){
        Container<Product> c1 = new Container<Product>();
        c1.setItem(new Phone());
        c1.setItem(new Camera());
        System.out.println(c1.getItem(0).getName());
    }
}
